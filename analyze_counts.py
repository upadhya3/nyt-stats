from collections import Counter,defaultdict
import sys
import operator

def analyzeCounts(filename):
    lines=[map(lambda x: x.strip("(").strip(")").strip(","),l.strip().split()) for l in open(filename).readlines()]
    frameCnt= Counter()
    frame2triggers = defaultdict(list)
    for l in lines:
        # print l
        frameCnt.update(dict([(l[1],float(l[2]))]))
        frame2triggers[l[1]]+=[l[0]]
    for c in frameCnt.most_common():
        print c[0],c[1]
        print frame2triggers[c[0]]
    # for k in frame2triggers:
    #     print k,frame2triggers[k]

def ACE_counts(filename):
    lines=[l.strip() for l in open(filename).readlines()]
    frame2triggers = defaultdict(list)
    for l in lines:
        # print l
        parts=l.strip().split(":")
        if len(parts[1])==0:
            continue
        frames=parts[1].split()
        for f in frames:
            frame2triggers[f]+=[parts[0]]

    # for k in frame2triggers:
    #     print k,frame2triggers[k]
    # print "==========================="
    sorted_frames = sorted(frame2triggers.items(), key=lambda x : len(x[1]), reverse=True)
    for f in sorted_frames:
        print f[0],Counter(frame2triggers[f[0]]).items()
        
if __name__=="__main__":
    # analyzeCounts(sys.argv[1])
    ACE_counts(sys.argv[1])
