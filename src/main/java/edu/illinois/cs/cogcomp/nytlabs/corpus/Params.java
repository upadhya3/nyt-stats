package edu.illinois.cs.cogcomp.nytlabs.corpus;

public class Params {
	public static final float OVERLAP_THRESHOLD = 0.4f;
	public static String path = "/shared/corpora/corporaWeb/written/eng/NYT_annotated_corpus/data/accum2003-07";// args[0];
	public static String trainAnnotations = "/home/upadhya3/nyt/nyt-train";
	public static String testAnnotations = "/home/upadhya3/nyt/nyt-eval";

	public static float cosineThresHold = 0.5f;
	public static String dumpPath = "mycache";
}
