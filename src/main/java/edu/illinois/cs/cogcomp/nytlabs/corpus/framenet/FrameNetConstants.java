
package edu.illinois.cs.cogcomp.nytlabs.corpus.framenet;

import java.util.HashSet;
import java.util.Set;

public class FrameNetConstants {
	 
	public static final String SubFrame = "SubFrame";
	public static final String SuperFrame = "SuperFrame";
	
	public static final Set<String> frameRelations = new HashSet<String>();
	static {
		frameRelations.add(SubFrame);
		frameRelations.add(SuperFrame);
	}
	
}




