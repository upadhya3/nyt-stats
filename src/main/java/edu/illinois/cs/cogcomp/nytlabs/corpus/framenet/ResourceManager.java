package edu.illinois.cs.cogcomp.nytlabs.corpus.framenet;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by upadhya3 on 11/10/15.
 */
public class ResourceManager {
    public static List<String> readLines(String resourceName, boolean trim) {
        String prefix = "";
        if(!resourceName.contains(Constants.RESOURCE_PREFIX)) {
            prefix = Constants.RESOURCE_PREFIX;
        }

        List<String> lines = new ArrayList<String>();

        try {
            InputStream is = ClassLoader.getSystemResourceAsStream(prefix+resourceName);
            if(is==null) {
                throw new Exception("Cannot load "+resourceName);
            }
            else {
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String line = null;
                while((line = br.readLine()) != null) {
                    if(trim)
                        lines.add(line.trim());
                    else
                        lines.add(line);
                }
                br.close();
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

        return lines;
    }

    public static List<String> readLines(String resourceName) {
        return readLines(resourceName, true);
    }

}
