//package edu.illinois.cs.cogcomp.nytlabs.corpus;
//
//import edu.illinois.cs.cogcomp.annotation.TextAnnotationBuilder;
//import edu.illinois.cs.cogcomp.annotation.handler.*;
//import edu.illinois.cs.cogcomp.comma.annotators.CommaLabeler;
//import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
//import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Annotator;
//import edu.illinois.cs.cogcomp.core.utilities.ResourceManager;
//import edu.illinois.cs.cogcomp.curator.CuratorAnnotator;
//import edu.illinois.cs.cogcomp.curator.CuratorClient;
//import edu.illinois.cs.cogcomp.curator.CuratorTextAnnotationBuilder;
//// import edu.illinois.cs.cogcomp.nlp.utilities.BasicAnnotatorService;
//import edu.illinois.cs.cogcomp.nlp.common.PipelineConfigurator;
//import edu.illinois.cs.cogcomp.nlp.tokenizer.IllinoisTokenizer;
//import edu.illinois.cs.cogcomp.nlp.util.SimpleCachingPipeline;
//import edu.illinois.cs.cogcomp.nlp.utilities.BasicTextAnnotationBuilder;
//import edu.illinois.cs.cogcomp.nlp.utility.CcgTextAnnotationBuilder;
//import edu.illinois.cs.cogcomp.srl.SemanticRoleLabeler;
//import edu.stanford.nlp.pipeline.POSTaggerAnnotator;
//import edu.stanford.nlp.pipeline.ParserAnnotator;
//
//import java.util.*;
//
//public class MyCuratorClient {
//
//	private static final String CONFIG_FILE = "config/caching-curator.properties";
//	public ResourceManager rm;
//	public SimpleCachingPipeline client;
//
//	public MyCuratorClient() throws
//			Exception {
//		rm = new ResourceManager(CONFIG_FILE);
//		TextAnnotationBuilder taBuilder = new CcgTextAnnotationBuilder(new IllinoisTokenizer());
//		ResourceManager defaultRM = new PipelineConfigurator().getConfig(rm);
//		List<Annotator> annotators = new ArrayList<>();
//		annotators.add(new IllinoisPOSHandler());
//		annotators.add(new IllinoisLemmatizerHandler(defaultRM));
//
//		Map<String, Annotator> viewProviders = new HashMap<>(annotators.size());
//
//		for (Annotator annotator : annotators) {
//			viewProviders.put(annotator.getViewName(), annotator);
//		}
//
//		client = new SimpleCachingPipeline(taBuilder, viewProviders, rm);
//	}
//
//}
