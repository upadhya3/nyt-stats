
package edu.illinois.cs.cogcomp.nytlabs.corpus.framenet;


public class LU {
	private final String lemma;
	private final String posType;
	 
	public LU(String lemma, String pos) {
		this.lemma = lemma;
		assert (pos.compareTo(Constants.NOUN)==0 || pos.compareTo(Constants.VERB)==0 || 
				pos.compareTo(Constants.ADJ)==0 || pos.compareTo(Constants.ADV)==0);
		this.posType = pos;
	}
	
	public String getLemma() {
		return lemma;
	}
	
	public String getPosType() {
		return posType;
	}
	
	public String toString() {
		String suffix = ".";
		if(posType.compareTo(Constants.NOUN)==0)
			suffix += "n";
		else if(posType.compareTo(Constants.VERB)==0)
			suffix += "v";
		return lemma+suffix;
	}
	
	public boolean equals(Object other) {
		if(this==other)
			return true;
		if( !(other instanceof LU) )
			return false;
		if(posType.compareTo(((LU)other).getPosType())==0 && lemma.compareTo(((LU)other).getLemma())==0)
			return true;
		else
			return false;
	}
	
	public int hashcode() {
		return new String(posType+"_"+lemma).hashCode();
	}
	
}

