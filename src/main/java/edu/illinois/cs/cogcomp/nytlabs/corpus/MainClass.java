package edu.illinois.cs.cogcomp.nytlabs.corpus;

import edu.illinois.cs.cogcomp.annotation.AnnotatorException;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TokenLabelView;
import edu.illinois.cs.cogcomp.core.stats.Counter;
import edu.illinois.cs.cogcomp.core.stats.OneVariableStats;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.NYTCorpusDocument;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.NYTCorpusDocumentParser;
import edu.illinois.cs.cogcomp.nytlabs.corpus.framenet.Frame;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class MainClass {

	public static void main(String[] args) throws Exception {
		ExtractAbstracts(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
		System.exit(-1);
	}

//	static TextAnnotation annotate(String text, MyCuratorClient cc, boolean isAbstract) throws AnnotatorException {
//
//		if (isAbstract) {
//			text = text.split(";")[0] + ".";
//			System.out.println("ABSTRACT TO ANNOTATE:" + text);
//			TextAnnotation ta = cc.client.createAnnotatedTextAnnotation("nyt", "" + "_abs", text);
//			// System.out.println(cleanText);
//			for (String viewName : cc.rm
//					.getCommaSeparatedValues("viewsToAdd")) {
//				cc.client.addView(ta, viewName);
//			}
//			return ta;
//		}
//		TextAnnotation ta = cc.client.createAnnotatedTextAnnotation("nyt", "", text);
//		// System.out.println(cleanText);
//		for (String viewName : cc.rm
//				.getCommaSeparatedValues("viewsToAdd")) {
//			cc.client.addView(ta, viewName);
//		}
//		return ta;
//	}

	static Counter<Pair<String,String>> posCountsBody = new Counter<>();
	static Counter<Pair<String,String>> posCountsAbs = new Counter<>();

	static int confident=0;

//	private static void frameTriggersCheap(TextAnnotation bodyTa,String prefix) {
//		TokenLabelView pos = (TokenLabelView) bodyTa.getView(ViewNames.POS);
//		for (Constituent tok : pos.getConstituents()) {
//			if(tok.getLabel().contains("NN")) {
//				List<Pair<String, String>> tmp = new ArrayList<>();
//				System.out.println(tok + " " + "NOUN");
//				tmp.add(new Pair<String, String>("NOUN", tok.getSurfaceForm()));
//				for(Frame frame:fm.getRelevantFrames(tmp, 1))
//				{
//					System.out.println(prefix+" "+tok+" "+frame.getName());
//				}
//			}
//			if(tok.getLabel().contains("VB")) {
//				List<Pair<String, String>> tmp = new ArrayList<>();
//				System.out.println(tok + " " + "VERB");
//				tmp.add(new Pair<String, String>("VERB", tok.getSurfaceForm()));
//				for(Frame frame:fm.getRelevantFrames(tmp, 1))
//				{
//					System.out.println(prefix+" "+tok+" "+frame.getName());
//				}
//			}
//		}
//	}


	//	static FrameNetManager fm = new FrameNetManager("framenet_1.5");
	public static void ExtractAbstracts(final int limit, int thres) throws Exception {
		final NYTCorpusDocumentParser pp = new NYTCorpusDocumentParser();
		final int[] cnt = {0};
		Path startPath = Paths.get(Params.path);
		final Counter<String> desc= new Counter<>();
		Files.walkFileTree(startPath, new SimpleFileVisitor<Path>() {
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
					throws IOException {
				String textFile = file.toString();
				NYTCorpusDocument nydoc = pp.parseNYTCorpusDocumentFromFile(new File(textFile), false);
				if (isGood(nydoc)) {
					for(String j : nydoc.getOnlineDescriptors())
						desc.incrementCount(j);

					cnt[0]++;
					if (cnt[0] % 100 == 0) {
						System.out.println(cnt[0]);
					}
					if (cnt[0] == limit) {
						System.out.println("Done!");
						return FileVisitResult.TERMINATE;
					}
				}
				return FileVisitResult.CONTINUE;
			}
		});
		System.out.println("HEY!!");
		System.out.println(desc.getTotal());
		for(String str:desc.getSortedItemsHighestFirst())
		{
			if(desc.getCount(str)>thres)
			System.out.println(str+" "+desc.getCount(str));
		}
	}

	private static void UpdateCountPOS(TextAnnotation ta, Counter<Pair<String, String>> c) {
		TokenLabelView pos = (TokenLabelView) ta.getView(ViewNames.POS);
		for(Constituent cons:pos.getConstituents())
		{
			c.incrementCount(new Pair<String,String>(cons.getSurfaceForm(),cons.getLabel()));
		}
	}

	private static boolean isGood(NYTCorpusDocument nydoc) {
		boolean section = false;
		boolean desc = false;
		boolean abs = false;
		boolean headline = false;
//		System.out.println(nydoc.getOnlineSection());

		// Uncomment to get pages which have both!
//		if(nydoc.getOnlineSection()!=null && nydoc.getOnlineSection().contains("World") && nydoc.getOnlineSection().contains("Front Page"))
//		{
//			section=true;
//		}

		if (nydoc.getOnlineSection() != null && nydoc.getOnlineSection().contains("World")) {
			section = true;
		}
		if (nydoc.articleAbstract != null && nydoc.articleAbstract.length()>=20 && !IsBadAbstract(nydoc.articleAbstract)) {
			abs = true;
		}
		if (!nydoc.getDescriptors().contains("Public Opinion") && !nydoc.getDescriptors().contains("Biographical Information") && !nydoc.getDescriptors().contains("Social Conditions and Trends")) {
			desc = true;
		}
		if (nydoc.headline != null && !nydoc.headline.contains("World Briefing |")) {
			headline = true;
		}
		if (section && desc && abs && headline)
			return true;
		return false;
	}

	public static boolean stringContainsItemFromList(String inputString, String[] items) {
		for (int i = 0; i < items.length; i++) {
			if (inputString.contains(items[i])) {
				return true;
			}
		}
		return false;
	}

	static String[] bad = {"Transcript of ", "Times column", "analysis;", "Article in", "Analysis:", "gives recipe", "Article desribes", "column notes", "article considers", "Excerpts from", "Interview with", "interview with", "interview of", "reviews", "column on", "Analysis of", "analysis of", "column says", "Op-Ed column", "Op-Ed article", "column discusses", "Answer to reader's query", "Profile of", "profile of", "Advice on", "Article on", "Article by", "article by", "Study by", "comments on", "Update on", "Editorial on", "Editorial", "Article describes"};

	public static boolean IsBadAbstract(String s) {
		return stringContainsItemFromList(s, bad);
	}
}
