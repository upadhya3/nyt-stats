
package edu.illinois.cs.cogcomp.nytlabs.corpus.framenet;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.io.LineIO;

import java.io.FileNotFoundException;
import java.util.*;

public class FrameNetManager {
	public static Map<String, Frame> frames;				// frame name -> Frame
	public static Map<Pair<String,String>, Set<Frame>> luToFrame;		// <posType, lexical unit string> -> Frames
	private static String FNHOME;

	public FrameNetManager(String fnhome) {
		FNHOME=fnhome;
		frames = new HashMap<String, Frame>();
		luToFrame = new HashMap<Pair<String,String>, Set<Frame>>();

		try {
			readFrames();
			readLexicalUnits();
			mapLUToFrame();
			readFrameRelations();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}


	}


	/**
	 * Go down the descendant tree and find all children frames
	 *
	 * @param f		The parent frame to start from
	 * @return		The set of all children/descendant frames
	 */
	public Set<Frame> getAllChildrenFrames(Frame f, Set<Frame> seenFrames) {
		Set<Frame> resultFrames = new HashSet<Frame>();

//		System.out.println("Parent frame: "+f.getName());
		Set<String> relatedFrames = f.getRelatedFrames(FrameNetConstants.SubFrame);
//		System.out.println("Children frames:");
//		if(relatedFrames!=null) {
//			for(Iterator<String> it=relatedFrames.iterator(); it.hasNext();) {
//				System.out.print(" ["+it.next()+"]");
//			}
//			System.out.println("");
//		}

		if(relatedFrames!=null) {
			for(Iterator<String> it=relatedFrames.iterator(); it.hasNext();) {
				String frameName = it.next();
				Frame frame = frames.get(frameName);
				if(frame!=null && !seenFrames.contains(frame)) {
					resultFrames.add(frame);
					seenFrames.add(frame);
					//System.out.println("Going in");
					resultFrames.addAll( getAllChildrenFrames(frame, seenFrames) );
				}
			}
		}

		return resultFrames;
	}

	public Set<Frame> getDirectChildrenFrames(Frame f) {
		Set<Frame> resultFrames = new HashSet<Frame>();

		Set<String> relatedFrames = f.getRelatedFrames(FrameNetConstants.SubFrame);
		for(Iterator<String> it=relatedFrames.iterator(); it.hasNext();) {
			String frameName = it.next();
			Frame frame = frames.get(frameName);
			if(frame!=null) {
				resultFrames.add(frame);
			}
		}

		return resultFrames;
	}

	public Set<Frame> getDirectParentFrames(Frame f) {
		Set<Frame> resultFrames = new HashSet<Frame>();

		Set<String> relatedFrames = f.getRelatedFrames(FrameNetConstants.SuperFrame);
		for(Iterator<String> it=relatedFrames.iterator(); it.hasNext();) {
			String frameName = it.next();
			Frame frame = frames.get(frameName);
			if(frame!=null) {
				resultFrames.add(frame);
			}
		}

		return resultFrames;
	}

	/**
	 * Gather a set of relevant frames based on an input list of lexical units. However, if I cannot find any relevant frames based on 'n', I will default to n=1.
	 *
	 * @param lus	List of <posType,lemma> lexical units
	 * @param n		A frame is only relevant if it contains at least 'n' elements in lus 
	 * @return		A set of relevant frames
	 */
	public Set<Frame> getRelevantFrames(List<Pair<String,String>> lus, int n) {
		Set<Frame> resultFrames = new HashSet<Frame>();

		// first, gather the entire set of candidate frames
		Set<Frame> candidateFrames = new HashSet<Frame>();
		for(int i=0; i<lus.size(); i++) {
			Pair<String,String> lu = lus.get(i);
			if(luToFrame.containsKey(lu)) {
				candidateFrames.addAll(luToFrame.get(lu));
			}
		}

		for(Iterator<Frame> frameIt=candidateFrames.iterator(); frameIt.hasNext();) {
			// count how many input lexical units this frame contains
			Frame frame = frameIt.next();
			int c = 0;
			for(int i=0; i<lus.size(); i++) {
				String posType = lus.get(i).getFirst();
				String lemma = lus.get(i).getSecond();
				if(frame.hasLU(posType, lemma)) {
					c += 1;
				}
			}
			if(c >= n) {
				resultFrames.add(frame);
			}
		}

		if(resultFrames.size() >= 1)
			return resultFrames;
		else
			return candidateFrames;
	}


	private static void mapLUToFrame() {
		for(Iterator<String> it=frames.keySet().iterator(); it.hasNext();) {
			String frameName = it.next();
			Frame frame = frames.get(frameName);

			Set<LU> lus = frame.getAllLu();
			for(Iterator<LU> luIt=lus.iterator(); luIt.hasNext();) {
				LU lu = luIt.next();
				Pair<String,String> p = new Pair<String,String>(lu.getPosType(), lu.getLemma());
				Set<Frame> myframes = null;
				if(!luToFrame.containsKey(p)) {
					myframes = new HashSet<Frame>();
				}
				else {
					myframes = luToFrame.get(p);
				}
				myframes.add(frame);
				luToFrame.put(p, myframes);
			}
		}
	}

	private static void readFrames() throws FileNotFoundException {
		List<String> lines = LineIO.read(FNHOME + "/" + "frameIndex.xml");

		for(String line:lines) {
			line=line.trim();
			if(line.startsWith("<frame ")) {
				String frameName = Util.getXMLAttributeValue(line, "name");
				Frame frame = new Frame(frameName);
				frames.put(frameName, frame);
			}
		}
	}

	private static void readLexicalUnits() throws FileNotFoundException{
		List<String> lines = LineIO.read(FNHOME + "/" + "luIndex.xml");

		for(String line:lines) {
			line=line.trim();
			if(line.startsWith("<lu ")) {
				String frameName = Util.getXMLAttributeValue(line, "frameName");
				assert frames.containsKey(frameName);

				String luName = Util.getXMLAttributeValue(line, "name");
				String lemma = luName.substring(0, luName.lastIndexOf("."));
				lemma = lemma.replaceAll("\\(", "");
				lemma = lemma.replaceAll("\\)", "");
				lemma = lemma.replaceAll(" ", "_");

				String posType = null;
				if(luName.endsWith(".n"))
					posType = Constants.NOUN;
				else if(luName.endsWith(".v"))
					posType = Constants.VERB;

				if( posType!=null && (posType.compareTo(Constants.NOUN)==0 || posType.compareTo(Constants.VERB)==0) ) {
					LU lu = new LU(lemma, posType);
					frames.get(frameName).addLu(lu);
				}
			}
		}
	}

	private static void readFrameRelations() throws FileNotFoundException{
		List<String> lines = LineIO.read(FNHOME + "/" + "frRelation.xml");

		for(String line:lines) {
			line=line.trim();
			if(line.startsWith("<frameRelation ")) {
				String subFrame = Util.getXMLAttributeValue(line, "subFrameName");
				String superFrame = Util.getXMLAttributeValue(line, "superFrameName");
				assert frames.containsKey(subFrame);
				assert frames.containsKey(superFrame);

				frames.get(subFrame).addRelation(FrameNetConstants.SuperFrame, superFrame);
				frames.get(superFrame).addRelation(FrameNetConstants.SubFrame, subFrame);
			}
		}
	}



	public static void printFrames() {
		for(Iterator<Frame> it=frames.values().iterator(); it.hasNext();) {
			System.out.println(it.next());
		}
	}

	public static void main(String[] args) throws FileNotFoundException {
		FrameNetManager fm = new FrameNetManager("framenet_1.5");
//		System.out.println(fm.frames.size());
		fm.printFrames();
//		int c=0;
//		for(Frame f: frames.values())
//		{
//			System.out.println(f.getAllLu().size());
//			if(f.getAllLu().size()>=10)
//			{
//				c++;
//			}
//		}
		ArrayList<Pair<String, String>> tmp = new ArrayList<Pair<String, String>>();
		tmp.add(new Pair<String, String>("VERB","thicken"));
		Set<Frame> ff = fm.getRelevantFrames(tmp, 1);
		System.out.println(ff);
//		for(Pair<String,String> k:luToFrame.keySet())
//		{
//			System.out.println(k);
//		}
//		System.out.println(c);
	}


}
