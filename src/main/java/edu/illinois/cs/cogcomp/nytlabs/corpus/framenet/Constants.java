
package edu.illinois.cs.cogcomp.nytlabs.corpus.framenet;

import java.util.HashSet;
import java.util.Set;

public class Constants {
	
	public static final String PROPERTY_FILENAME = "coreUtil.properties";
	
	public static final String NOUN = "NOUN";
	public static final String VERB = "VERB";
	public static final String ADJ = "ADJ";
	public static final String ADV = "ADV";
	public static final String MISC = "MISC";
	
	public static final String RESOURCE_PREFIX = "coreUtil/";
	
	
	public static final String NE_MENTION = "NE_MENTION";
	public static final String VALUE_MENTION = "VALUE_MENTION";
	public static final String TIME_MENTION = "TIME_MENTION";
	public static final String PREDICATE = "PREDICATE";
	public static final String NOT_MENTION = "NULL";
	
	public static final String GOLD_NE_MENTION_VIEW = "GOLD_NE_MENTION_VIEW";
	public static final String GOLD_VALUE_MENTION_VIEW = "GOLD_VALUE_MENTION_VIEW";
	public static final String GOLD_TIME_MENTION_VIEW = "GOLD_TIME_MENTION_VIEW";
	public static final String GOLD_PREDICATE_VIEW = "GOLD_PREDICATE_VIEW";
	public static final String CANDIDATE_NE_MENTION_VIEW = "CANDIDATE_NE_MENTION_VIEW";
	public static final String CANDIDATE_PREDICATE_VIEW = "CANDIDATE_PREDICATE_VIEW";
	public static final String TYPED_CANDIDATE_NE_MENTION_VIEW = "TYPED_CANDIDATE_NE_MENTION_VIEW";
	public static final String PRED_NE_MENTION_VIEW = "PRED_NE_MENTION_VIEW";
	public static final String PRED_PREDICATE_VIEW = "PRED_PREDICATE_VIEW";
	
	public static final String ALL_EVENT_MENTION = "ALL_EVENT_MENTION";	// includes STATE
	public static final String EVENT_MENTION = "EVENT_MENTION";			// excludes STATE
	public static final String EVENT_MENTION_CANDIDATE = "EVENT_MENTION_CANDIDATE";
	public static final String NOT_EVENT_MENTION = "NULL";
	
	public static final String GOLD_EVENT_PREDICATE_ARGUMENT_VIEW = "GOLD_EVENT_PREDICATE_ARGUMENT_VIEW";
	public static final String PRED_EVENT_PREDICATE_ARGUMENT_VIEW = "PRED_EVENT_PREDICATE_ARGUMENT_VIEW";
	
	public static final String NO_RELATION = "NO_RELATION";
	public static final String PRED_RELATION_VIEW = "PRED_RELATION_VIEW";
	public static final String GOLD_RELATION_VIEW = "GOLD_RELATION_VIEW";
	
	
	public static Set<String> setXMLTags = new HashSet<String>();
	static {
		setXMLTags.add("ANNOTATION");
		setXMLTags.add("BODY");
		setXMLTags.add("DATE");
		setXMLTags.add("DATETIME");
		setXMLTags.add("DATE_TIME");
		setXMLTags.add("DOC");
		setXMLTags.add("DOCID");
		setXMLTags.add("DOCNO");
		setXMLTags.add("DOCTYPE");
		setXMLTags.add("ENDTIME");
		setXMLTags.add("END_TIME");
		setXMLTags.add("FOOTER");	// not found in either ace04, ace05, nor IC
		setXMLTags.add("HEADER");
		setXMLTags.add("HEADLINE");
		setXMLTags.add("KEYWORD");	// not found in either ace04, ace05, nor IC
		setXMLTags.add("P");
		setXMLTags.add("POST");
		setXMLTags.add("POSTDATE");
		setXMLTags.add("POSTER");
		setXMLTags.add("QUOTE");
		setXMLTags.add("SLUG");
		setXMLTags.add("SPEAKER");
		setXMLTags.add("SUBJECT");
		setXMLTags.add("TEXT");
		setXMLTags.add("TRAILER");
		setXMLTags.add("TURN");
	}
	
}