#mvn -q dependency:copy-dependencies
#mvn -q compile
CP="./config/:./target/classes/:./target/dependency/*"

MEMORY="-Xmx10g"
# OPTIONS="$MEMORY -Xss40m -ea -cp $CP"
OPTIONS="$MEMORY -Xss40m -ea -cp $CP"
PACKAGE_PREFIX="edu.illinois.cs.cogcomp"

MAIN="$PACKAGE_PREFIX.nytlabs.corpus.ACEEvents"
MAIN="$PACKAGE_PREFIX.nytlabs.corpus.core.NYTAnnotations"
MAIN="$PACKAGE_PREFIX.nytlabs.corpus.MainClass"
#MAIN="$PACKAGE_PREFIX.nytlabs.corpus.SRLTesting"
#MAIN="$PACKAGE_PREFIX.nytlabs.corpus.core.NYTCorpusDocument"

# MAIN="$PACKAGE_PREFIX.salience.learning.Main"
# MAIN="$PACKAGE_PREFIX.nytlabs.corpus.MainClass"

time nice java $OPTIONS $MAIN $CONFIG_STR $*
